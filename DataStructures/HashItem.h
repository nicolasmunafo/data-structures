#pragma once


//Item that will be part of hash tables
template<class T>
class HashItem
{
public:
	HashItem() :m_key{ -1 } { }
	HashItem(int keyValue, T item): m_key { keyValue }, m_itemData { item } { }

	~HashItem() { }

	int GetKey() { return m_key;}

	void SetKey(int keyValue) { m_key = keyValue;}

	T GetData() { return m_itemData; }		//The book does not return a reference because a reference may try to modify the values which are private

	void SetData(T newItem) { m_itemData = newItem; }

	void operator=(HashItem& otherHashItem) {
		m_key = otherHashItem.m_key;
		m_itemData = otherHashItem.m_itemData;
	}

	bool operator==(HashItem& otherHashItem) {
		return (m_key == otherHashItem.GetKey());
	}

private:
	int m_key;			//key for calculating the hash value
	T m_itemData;		//Data of the hash table index

};

