#pragma once
#include "BinaryTree.h"

template<class T>
class BinarySearchTree
{
public:
	BinarySearchTree(int (*compFunc)(T, T)) : m_root(nullptr), CompareFunction (compFunc) {	}

	~BinarySearchTree() {
		if (m_root != nullptr) {
			delete m_root;
			m_root = nullptr;
		}
	}

	//Insert an item in the tree
	bool Insert(T newData) {
		if (m_root == nullptr) {						//If root is null, then the first insert will be the root
			m_root = new BinaryTree<T>(newData);
			return true;
		}
		else {
			//bool isLeftNode = false;
			BinaryTree<T>* itr = m_root;				//If root is not null, create an iterator starting from the root
			while (true) {
				int compareValue = CompareFunction(newData, itr->GetData());
				if (compareValue < 0) {	//If the comparison is negative, the data must go to the left
					if (itr->m_left == nullptr) {
						itr->m_left = new BinaryTree<T>(newData);
						return true;
					}
					itr = itr->m_left;
				}
				else  {
					if (compareValue > 0) { //If the comparison is 0 or greater the data must go to the right
						if (itr->m_right == nullptr) {		//If the right node is null, then the data must be inserted at the right of the iterator
							itr->m_right = new BinaryTree<T>(newData);
							return true;
						}
						itr = itr->m_right;
					} else {					//The data is already in the tree so it will not be inserted
						return false;
					}											
				}
			}
		}
	}

	void PreOrder() {
		m_root->PreOrder();
	}

	void InOrder() {
		m_root->InOrder();
	}

	void PostOrder() {
		m_root->PostOrder();
	}

	//Search for an item in the tree
	bool Search(T key) {
		BinaryTree<T>* itr = m_root;	//Start from the root of the tree
		int compareValue = CompareFunction(key, itr->GetData());
		while (compareValue != 0) {			//Repeat while the key and the data of the iterator are different
			if (compareValue < 0) {
				itr = itr->m_left;
			}
			else {
				itr = itr->m_right;
			}
			if (itr == nullptr) {
				return false;
			}
			else {
				compareValue = CompareFunction(key, itr->GetData());
			}
		}
		return true;
		
	}

private:
	BinaryTree<T>* m_root;				//Root pointing to Binary Tree
	int (*CompareFunction)(T a, T b);		//Pointer to a function which will compare two T values.
											//If returns <0, a<b | =0 => a=b | >0 => a>b
};

