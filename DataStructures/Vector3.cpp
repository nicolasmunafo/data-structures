#include "Vector3.h"

Vector3 Vector3::operator+(Vector3 otherVector) {
	return Vector3(x + otherVector.x, y + otherVector.y, z + otherVector.z);
}