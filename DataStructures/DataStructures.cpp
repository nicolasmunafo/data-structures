// DataStructures.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include "LinkList.h"
#include "LinkIterator.h"
#include "StackArray.h"
#include "StackList.h"
#include "Queue.h"
#include "PriorityQueue.h"
#include "HashTableLP.h"
#include "HashTableSC.h"
#include "Tree.h"
#include "TreeIterator.h"
#include "BinaryTree.h"
#include "BinarySearchTree.h"
#include "Heap.h"
#include "Graph.h"
#include "Vector3.h"

using namespace std;

#pragma region Function_Definitions
void testUnorderedArray();
void testOrderedArray();
void testLinkList();
void testStackArray();
void testStackList();
void testQueue();
void testLinkInsert();
void testPriorityQueue();
void testPrimality();
void testHashTableLP();
void testHashTableSC();
void testGeneralTree();
void testBinaryTree();
void testBinarySearchTree();
void testHeap();
void testGraph();
void testPathfindingAlgorithms();
void testAStartAlgorithm();

bool IsNumPrime(int num) {
    for (int i=2; (i * i) <= num; i++) {
        if (num % i == 0) {
            return false;
        }
    }
    return true;
}

#pragma endregion Function_Definitions

int main()
{
    //Arrays
    //testUnorderedArray();
    //testOrderedArray();

    //Link lists, stacks and queues
    //testLinkList();
    //testStackArray();
    //testStackList();
    //testQueue();
    //testLinkInsert();
    //testPriorityQueue();

    /*HashTables*/
    //testHashTableLP();
    //testHashTableSC();

    /*Trees*/
    //testGeneralTree();
    //testBinaryTree();
    //testBinarySearchTree();
    //testHeap();

    /*Graphs*/
    //testGraph();
    //testPathfindingAlgorithms();
    testAStartAlgorithm();

}

#pragma region Arrays
void testUnorderedArray() {
    UnorderedArray<typename int> numbersList = UnorderedArray<typename int>(3, 6);
    int number;

    int value;
    cin >> number;
    numbersList.push(number);
    numbersList.push(3);
    numbersList.push(5);
    numbersList.push(3);
    numbersList.push(7);
    //numbersList.remove(1);
    for (int i = 0; i < (numbersList.numElements);i++) {
        cout << numbersList[i];
    }

    cout << numbersList.maxSize;

    cout << "Search for a value: "; cin >> value;
    int searchRes = numbersList.searchItem(value);
    if (searchRes >= 0) {
        cout << "Value was found in position: " << searchRes;
    }
    else {
        cout << "Value not found!";
    }

    

}

void testOrderedArray() {
    OrderedArray<typename int> numbersList = OrderedArray<typename int>(3, 6);
    int number;
    int value;

    cin >> number;
    numbersList.push(number);
    numbersList.push(12);
    numbersList.push(9);
    numbersList.push(20);
    numbersList.push(18);

    for (int i = 0; i < (numbersList.numElements);i++) {
        cout << numbersList[i] <<" | ";
    }

    cout << "Search for a value: "; cin >> value;
    int searchRes = numbersList.searchItem(value);
    if (searchRes >= 0) {
        cout << "Value was found in position: " << searchRes;
    }
    else {
        cout << "Value not found!";
    }
}

#pragma endregion Arrays

#pragma region linklist
void testLinkList() {
    LinkList<typename int> numbersList;

    numbersList.Push(4);
    numbersList.Push(7);
    numbersList.Push(94);

    numbersList.PushFront(20);
    numbersList.PushFront(8);
    numbersList.Pop();
    numbersList.PopFront();
    numbersList.Push(15);
    numbersList.PopFront();

    LinkIterator<int> numbListIt;
    numbListIt = numbersList.GetRoot();
    numbListIt = numbersList.GetRoot();
    while (numbListIt != nullptr) {
        cout << *numbListIt << " | ";
        ++numbListIt;
    }

}
#pragma endregion linklist

#pragma region Stacks_Queues
void testStackArray() {
    StackArray<typename int> numbersList = StackArray<typename int>(4, 2);

    numbersList.Push(4);
    numbersList.Push(7);
    numbersList.Push(94);

    numbersList.Pop();
    numbersList.Push(15);

    while (numbersList.GetSize()>0) {
        
        cout << numbersList.Top() << " | ";
        numbersList.Pop();
    }
}

void testStackList() {
    StackList<typename int> numbersList;

    numbersList.Push(4);
    numbersList.Push(7);
    numbersList.Push(94);
    numbersList.Pop();
    numbersList.Push(15);


    while (numbersList.GetSize() > 0) {

        cout << numbersList.Top() << " | ";
        numbersList.Pop();
    }
}

void testQueue() {
    Queue<typename int> numbersList = Queue<int>(20);

    numbersList.PushFront(4);
    numbersList.PushBack(7);
    numbersList.PushFront(94);
    numbersList.PopBack();
    numbersList.PushFront(15);


    while (numbersList.GetSize() > 0) {

        cout << numbersList.Front() << " | ";
        numbersList.PopFront();
    }
}

void testLinkInsert() {
    LinkList<typename int> numbersList;

    numbersList.Push(4);
    numbersList.Push(7);
    numbersList.Push(94);

    LinkIterator<int> numbListIt;
    numbListIt = numbersList.GetRoot();
    numbListIt++;
    numbListIt++;
    //numbListIt++;

    numbersList.InsertBefore(numbListIt, 20);

    numbListIt = numbersList.GetRoot();
    while (numbListIt != nullptr) {
        cout << *numbListIt << " | ";
        ++numbListIt;
    }

    numbListIt = numbersList.GetRoot();
    cout << *numbListIt;

}

#pragma endregion Stacks_Queues

#pragma region PriorityQueues
template<typename T>
class less_cmp {
public:
    bool operator()(T lval, T rval) {
        return (lval < rval);
    }
};

class NetworkMessage {
public:
    NetworkMessage() : m_priority{ 0 }, m_message{ "text" } { };
    NetworkMessage(int priority, string newMessage) : m_priority{ priority }, m_message{ newMessage } { }

    bool operator<(NetworkMessage &otherMessage) {
        return (m_priority < otherMessage.GetPriority());
    }

    int GetPriority() {
        return m_priority;
    }

    string GetMessage() {
        return m_message;
    }

private:
    int m_priority;
    string m_message;
};

void testPriorityQueue() {
    
    NetworkMessage message1(4, "text 4");
    NetworkMessage message2(8, "text 8");
    NetworkMessage message3(15, "text 15");
    NetworkMessage message4(6, "text 6");
    NetworkMessage message5(20, "text 20");

    //Create a new priority queue with two classes:
    //1- Network Message, containing the data
    //2- Less comparison class, which compares network messages
    PriorityQueue<NetworkMessage, less_cmp<NetworkMessage>> messageQueue(10);

    messageQueue.Push(message1);
    messageQueue.Push(message5);
    messageQueue.Push(message3);
    messageQueue.Push(message4);
    messageQueue.Push(message2);


    while (messageQueue.GetSize() > 0) {
        cout << messageQueue.Front().GetMessage();
        messageQueue.Pop();
    }

    cout << (messageQueue.IsEmpty() ? "Empty" : "Not Empty");
    
}

#pragma endregion PriorityQueues

#pragma region HashTables
/**********************************************************************

Hash tables

***********************************************************************/

void testPrimality() {
    int num;
    cout << "Enter number to test primality";
    cin >> num;
    while (num > 0) {
        if (IsNumPrime(num)) {
            cout << "It is prime";
        }
        else {
            cout << "It is not prime";
        }
        cout << "Enter number to test primality";
        cin >> num;

    }
}

void testHashTableLP() {
    HashTableLP<string> hashTableText(20);

    string text = "initial text";

    //hashTableText.Insert(5, text);
    hashTableText.Insert(6, text);
    hashTableText.Insert(11, text);
    hashTableText.Insert(21, text);
    hashTableText.Insert(36, text);
    text = "Text 51";
    hashTableText.Insert(51, text);

    hashTableText.Delete(11);
    //hashTableText.Delete(51);
    //hashTableText.Delete(50);
    //hashTableText.Delete(11);

    for (int i = 0; i < hashTableText.GetSize(); i++) {
        cout << hashTableText.m_hashTable[i].GetKey() << endl;
    }

    //string* returnText = new string;
    string returnText;
    cout << hashTableText.Find(51, &returnText) << returnText << endl;

    cout << hashTableText.HashFunction("cats");

}

void testHashTableSC() {
    HashTableSC<string> hashTableText(20);

    string text = "initial text";

    //hashTableText.Insert(5, text);
    hashTableText.Insert(5, text);
    text = "initial text 2";
    hashTableText.Insert(11, text);
    text = "initial text 3";
    hashTableText.Insert(21, text);

    hashTableText.Delete(2);

    string retText;
    hashTableText.Find(5, &retText);
    cout << retText;
    
    //hashTableText.Delete(2);


    /*hashTableText.Insert(36, text);
    text = "Text 51";
    hashTableText.Insert(51, text);

    hashTableText.Delete(11);
    //hashTableText.Delete(51);
    //hashTableText.Delete(50);
    //hashTableText.Delete(11);

    for (int i = 0; i < hashTableText.GetSize(); i++) {
        cout << hashTableText.m_hashTable[i].GetKey() << endl;
    }*/

    //string* returnText = new string;
    //string returnText;
    //cout << hashTableText.Find(51, &returnText) << returnText << endl;



}

#pragma endregion HashTables

#pragma region Trees
/*********************************************************************

Trees

*********************************************************************/
void testGeneralTree() {
    Tree<int>* root= new Tree<int>(1);
    Tree<int>* subTreeA = new Tree<int>(2);
    Tree<int>* subTreeB = new Tree<int>(4);
    root->AddChild(subTreeA);
    root->AddChild(subTreeB);
    subTreeA->AddChild(new Tree<int>(5));
    subTreeA->AddChild(new Tree<int>(6));
    subTreeA->AddChild(new Tree<int>(7));

    TreeIterator<int> treeIt(root);
    cout << *treeIt << endl;
    treeIt.Down();
    cout << *treeIt << endl;
    treeIt.Down();
    cout << *treeIt << endl;
    treeIt.Root();
    cout << *treeIt << endl;


}

void testBinaryTree() {
    BinaryTree<int>* root;
    BinaryTree<int>* itr;

    root = new BinaryTree<int>(1);
    root->SetLeftChild(new BinaryTree<int>(2));
    root->SetRightChild(new BinaryTree<int>(3));
    itr = root->GetLeftChild();
    itr->SetLeftChild(new BinaryTree<int>(4));
    itr->SetRightChild(new BinaryTree<int>(5));

    itr = root->GetRightChild();
    itr->SetLeftChild(new BinaryTree<int>(6));

    cout << "Root Data: " << root->GetData() << endl;
    cout << "Left child Data: " << root->GetLeftChild()->GetData() << endl;
    cout << "Right child Data: " << root->GetRightChild()->GetData() << endl;
    cout << "Current iterator data: " << itr->GetData() << endl;

    root->PreOrder(); cout << endl;
    root->InOrder(); cout << endl;
    root->PostOrder(); cout << endl;
}

int compareInteger(int a, int b) {
    return (a - b);
}

void testBinarySearchTree() {

    BinarySearchTree<int>* binTree = new BinarySearchTree<int>(compareInteger);
    binTree->Insert(20);
    binTree->Insert(15);
    binTree->Insert(10);
    binTree->Insert(25);
    binTree->Insert(17);
    binTree->Insert(24);
    binTree->Insert(30);

    binTree->PreOrder(); cout << endl;
    binTree->InOrder(); cout << endl;
    binTree->PostOrder(); cout << endl;
    
    int item;
    cout << "Enter a positive integer to search: "; cin >> item;
    while (item > 0) {
        (binTree->Search(item)) ? cout << "Found!" : cout << "Not found!";
        cout << "Enter a positive integer to search: "; cin >> item;
    }

}

#pragma endregion Trees

#pragma region Heap

void testHeap() {
    Heap<int>* numHeap = new Heap<int>;
    numHeap->Push(30);
    numHeap->Push(10);
    numHeap->Push(20);
    numHeap->Push(5);
    numHeap->Push(2);
    numHeap->Push(12);
    numHeap->Push(18);
    numHeap->Push(25);
    numHeap->Push(40);

    numHeap->Pop();
   numHeap->Pop();    numHeap->Pop();    numHeap->Pop();    numHeap->Pop();
   numHeap->Pop();
   numHeap->Pop();
   numHeap->Pop();
   numHeap->Pop();


    for (int i = 0; i < numHeap->Size(); i++) {
        cout << numHeap->m_heap[i] << " | ";
    }
}

#pragma endregion Heap

#pragma region Graphs

void testGraph() {
    Graph<int>* newGraph= new Graph<int>(6);
    newGraph->Push(0);
    newGraph->Push(1);
    newGraph->Push(2);
    newGraph->Push(3);
    newGraph->Push(4);
    newGraph->Push(5);

    //Print the data of each vertex
    for (int i = 0; i< newGraph->m_vertices.size(); i++) {
       cout << newGraph->m_vertices[i].GetNode() << " | ";
    }
    cout << endl;

    /*newGraph->AttachEdge(0, 0);
    newGraph->AttachEdge(0, 1);
    newGraph->AttachEdge(0, 3);
    newGraph->AttachEdge(1, 0);
    newGraph->AttachEdge(1, 2);
    newGraph->AttachEdge(1, 3);
    newGraph->AttachEdge(2, 1);
    newGraph->AttachEdge(3, 0);
    newGraph->AttachEdge(3, 1);*/

    /*
    newGraph->AttachEdge(0, 1);
    newGraph->AttachEdge(1, 3);
    newGraph->AttachEdge(1, 4);
    newGraph->AttachEdge(2, 2);
    newGraph->AttachEdge(2, 4);*/

    newGraph->AttachEdge(0, 1);
    newGraph->AttachEdge(0, 2);
    newGraph->AttachEdge(0, 3);
    newGraph->AttachEdge(2, 5);
    newGraph->AttachEdge(3, 4);
    newGraph->AttachEdge(2, 4);
    


    

    //Print the adjacency matrix
    for (int i = 0; i < 6; i++)
    {
        for (int j = 0; j < 6; j++)
        {
            cout << newGraph->m_adjMatrix[i][j] << " | ";
        }
        cout << endl;
    }

    newGraph->DepthFirstSearch(5, 1);

    newGraph->BreadthFirstSearch(5, 1);

    delete newGraph;


}

void testPathfindingAlgorithms() {
    int maxVerts = 9;

    Graph<char> levelGraph(maxVerts);

    //Insert the nodes
    levelGraph.Push('A');
    levelGraph.Push('B');
    levelGraph.Push('C');
    levelGraph.Push('D');
    levelGraph.Push('E');
    levelGraph.Push('F');
    levelGraph.Push('G');
    levelGraph.Push('H');
    levelGraph.Push('I');


    //Define adjacency
    levelGraph.AttachEdge(0, 3);        //A-B
    levelGraph.AttachEdge(0, 1);        //A-D
    levelGraph.AttachEdge(1, 2);        //B-C
    levelGraph.AttachEdge(2, 5);        //C-F
    levelGraph.AttachEdge(3, 6);        //D-G
    levelGraph.AttachEdge(3, 4);        //D-E
    levelGraph.AttachEdge(4, 5);        //E-F
    levelGraph.AttachEdge(5, 7);        //F-H
    levelGraph.AttachEdge(7, 8);        //H-I

    //levelGraph.AttachEdge(6, 8);        //G-I

    //Print the adjacency matrix
    for (int i = 0; i < maxVerts; i++)
    {
        for (int j = 0; j < maxVerts; j++)
        {
            cout << levelGraph.m_adjMatrix[i][j] << " | ";
        }
        cout << endl;
    }

    //Define the vectors of each node
    levelGraph.SetNodePosition(0, Vector3(0, 0, 0));
    levelGraph.SetNodePosition(1, Vector3(1, 0, 0));
    levelGraph.SetNodePosition(2, Vector3(2, 0, 0));
    levelGraph.SetNodePosition(3, Vector3(0, 1, 0));
    levelGraph.SetNodePosition(4, Vector3(1, 1, 0));
    levelGraph.SetNodePosition(5, Vector3(2, 1, 0));
    levelGraph.SetNodePosition(6, Vector3(0, 2, 0));
    levelGraph.SetNodePosition(7, Vector3(2, 2, 0));
    levelGraph.SetNodePosition(8, Vector3(2, 3, 0));

    levelGraph.GreedyBestFirstSearch(0, 8);

}

void testAStartAlgorithm() {
    int maxVerts = 19;

    Graph<char> levelGraph(maxVerts);

    
    //Insert the nodes
    levelGraph.Push('A');
    levelGraph.Push('B');
    levelGraph.Push('C');
    levelGraph.Push('D');
    levelGraph.Push('E');
    levelGraph.Push('F');
    levelGraph.Push('G');
    levelGraph.Push('H');
    levelGraph.Push('I');
    levelGraph.Push('J');
    levelGraph.Push('K');
    levelGraph.Push('L');
    levelGraph.Push('M');
    levelGraph.Push('N');
    levelGraph.Push('O');
    levelGraph.Push('P');
    levelGraph.Push('Q');
    levelGraph.Push('R');
    levelGraph.Push('S');

    //Define adjacency
    levelGraph.AttachEdge(0, 3);        //A-B
    levelGraph.AttachEdge(0, 1);        //A-D
    levelGraph.AttachEdge(1, 2);        //B-C
    levelGraph.AttachEdge(1, 4);        //B-C
    levelGraph.AttachEdge(2, 5);        //C-F
    levelGraph.AttachEdge(3, 6);        //D-G
    levelGraph.AttachEdge(3, 4);        //D-E
    levelGraph.AttachEdge(4, 5);        //E-F
    levelGraph.AttachEdge(4, 7);        //E-H
    levelGraph.AttachEdge(5, 8);        //F-I
    levelGraph.AttachEdge(6, 7);        //G-H
    levelGraph.AttachEdge(6, 9);        //G-J
    levelGraph.AttachEdge(7, 8);        //H-I

    levelGraph.AttachEdge(9, 10);       //J-K
    levelGraph.AttachEdge(10, 11);      //K-L
    levelGraph.AttachEdge(11, 12);      //L-M
    levelGraph.AttachEdge(12, 13);      //M-N
    levelGraph.AttachEdge(13, 14);        //N-O
    levelGraph.AttachEdge(14, 15);        //O-P
    levelGraph.AttachEdge(15, 16);        //P-Q
    levelGraph.AttachEdge(16, 17);        //Q-R
    levelGraph.AttachEdge(17, 18);        //R-S

    //Print the adjacency matrix
    for (int i = 0; i < maxVerts; i++)
    {
        for (int j = 0; j < maxVerts; j++)
        {
            cout << levelGraph.m_adjMatrix[i][j] << " | ";
        }
        cout << endl;
    }

    //Define the vectors of each node
    levelGraph.SetNodePosition(0, Vector3(0, 4, 0));
    levelGraph.SetNodePosition(1, Vector3(1, 4, 0));
    levelGraph.SetNodePosition(2, Vector3(2, 4, 0));
    levelGraph.SetNodePosition(3, Vector3(0, 3, 0));
    levelGraph.SetNodePosition(4, Vector3(1, 3, 0));
    levelGraph.SetNodePosition(5, Vector3(2, 3, 0));
    levelGraph.SetNodePosition(6, Vector3(0, 2, 0));
    levelGraph.SetNodePosition(7, Vector3(1, 2, 0));
    levelGraph.SetNodePosition(8, Vector3(2, 2, 0));
    levelGraph.SetNodePosition(9, Vector3(0, 1, 0));
    levelGraph.SetNodePosition(10, Vector3(0, 0, 0));
    levelGraph.SetNodePosition(11, Vector3(1, 0, 0));
    levelGraph.SetNodePosition(12, Vector3(2, 0, 0));
    levelGraph.SetNodePosition(13, Vector3(3, 0, 0));
    levelGraph.SetNodePosition(14, Vector3(4, 0, 0));
    levelGraph.SetNodePosition(15, Vector3(4, 1, 0));
    levelGraph.SetNodePosition(16, Vector3(4, 2, 0));
    levelGraph.SetNodePosition(17, Vector3(4, 3, 0));
    levelGraph.SetNodePosition(18, Vector3(4, 4, 0));


    levelGraph.GreedyBestFirstSearch(2, 17);    //Search path from D to R

    levelGraph.AStarSearch(2, 17);    //Search path from D to R


}

#pragma endregion Graphs

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
