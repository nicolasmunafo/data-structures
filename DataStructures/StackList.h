#pragma once
#include "LinkList.h"
#include "LinkIterator.h"



template<class T>
class StackList
{
public:
	StackList() {

	}

	~StackList() {
		
	}

	//Inserts an item at the end of the list (Push inserts at the end of the link list)
	void Push(T item) {
		m_container.Push(item);
	}

	//Removes an item from the end of the list (Pop removes from the end of the link list)
	void Pop() {
		m_container.Pop();
	}

	//Gets the last node from the link list and returns its data by reference
	const T& Top() {
		LinkIterator<T> listIterator;
		listIterator = m_container.GetLast();
		return *listIterator;
		//The following doesn't work because nodeData is private, you can use the dereference operator *
		//to access the data of "GetLast()"
		//return m_linkList->GetLast()->nodeData;
	}

	int GetSize() {
		return m_container.GetLength();
	}


private:
	LinkList<T> m_container;

};

