#pragma once

#include <vector>
#include <stack>
#include <deque>
#include <iostream>
#include "BinarySearchTree.h"
#include "GraphVertex.h"
#include <algorithm>

using namespace std;

template<class T>
class Graph
{
public:
	Graph(int maxVerts): m_maxVerts(0), m_adjMatrix(nullptr){
		if (maxVerts > 0) {
			m_maxVerts = maxVerts;

			//To create a multi dimensional array, first create an array of pointers to char
			m_adjMatrix = new char* [m_maxVerts];
			//Then for each item in the array, create an array of chars
			for (int i = 0; i < m_maxVerts; i++) {
				m_adjMatrix[i] = new char[m_maxVerts];
			}

			//This won't work because you cannot create a two dimensional array with new
			//m_adjMatrix = new char[1][m_maxVerts][m_maxVerts];
		}
	}

	~Graph() {
		if (m_adjMatrix != nullptr) {

			//To deallocate the memory first delete each dynamic array of each element of the vector
			for (int i = 0; i < m_maxVerts; i++) {
				delete[] m_adjMatrix[i];
				m_adjMatrix[i] = nullptr;
			}
			//Deallocate the memory of the rows vector
			delete[] m_adjMatrix;
			m_adjMatrix = nullptr;
		}
	}

	//Insert a vertex in the graph
	bool Push(T item) {
		if (m_vertices.size() < m_maxVerts) {			//Only insert an item if the vector has less vertices than the maximum allowed
			m_vertices.push_back(GraphVertex<T>(item, m_vertices.size()));	//put the item at the back of the vector adding the size of the vector as the index of the item. That is, if the vector was of size 2, then the item will be at position 2 and then it is pushed back
			return true;								//The item was placed correctly
		}
		return false;
	}

	//Attach an edge between the vertices at index "vertex1" and "vertex2"
	void AttachEdge(int vertex1, int vertex2) {
		if (m_adjMatrix != nullptr) {
			if (vertex1 < m_vertices.size() && vertex2 < m_vertices.size()) {		//Only perform if the vertex was already created, that is, if the index doesn't exceed the size of the vector
				m_adjMatrix[vertex1][vertex2] = '1';								//Set "true" to the element of the array corresponding to (vertex1;vertex2)
				m_adjMatrix[vertex2][vertex1] = '1';								//Set "true" to the element of the array corresponding to (vertex2;vertex1)
			}
		}
	}

	//Attach a directed edge from "vertex1" to "vertex2"
	void AttachDirectedEdge(int vertex1, int vertex2) {
		if (m_adjMatrix != nullptr) {
			if (vertex1 < m_vertices.size() && vertex2 < m_vertices.size()) {		//Only perform if the vertex was already created, that is, if the index doesn't exceed the size of the vector
				m_adjMatrix[vertex1][vertex2] = '1';								//Set "true" to the element of the array corresponding to (vertex1;vertex2)
			}
		}
	}

	//Depth first search from start vertex to end vertex. It returns a queue with the first item being the start vertex
	//And the last item being the end, if it finds a path
	//Depth first will always go with the first path that it finds (despite it being the longest)
	bool DepthFirstSearch(int startIndex, int endIndex) {
		int currentIndex = startIndex;					//Start from the start index

		vector<char> visitVertices(m_vertices.size());	//vector that holds a char indicating if the vertex of the associated index was visited or not
		
		visitVertices[currentIndex] = '1';				//The start vertex is marked as checked

		deque<int> pathVertices;						//Doble ended queue used as a stack which holds the path from the start of the search to the end
		
		pathVertices.push_back(currentIndex);			//Push the first item at the end of the queue (as a stack) to start from there

		while (currentIndex != endIndex && !pathVertices.empty()) {				//Repeat until it hasn't reached the end and while there are items in the deque
			
			currentIndex = pathVertices.back();									//First obtain the current index from the back (this goes here to avoid getting an element from an empty deque after popping the last element

			currentIndex = GetNextVertex(currentIndex, visitVertices);			//This obtains the next vertex that was not visited
			
			if (currentIndex == -1) {											//If there is no next vertex, it will pop the vertex and keep checking for the previous vertex
				pathVertices.pop_back();
				
			}
			else {																//If there is a vertex that has not been visited:
				
				visitVertices[currentIndex] = '1';								//The new index has to be set as visited
				
				pathVertices.push_back(currentIndex);							//The new vertex has to be pushed to the stack
			}
		}
		
		if (!pathVertices.empty()) {									//If deque is not empty, then it found a path, checking for "pathVertices.back() == endIndex" aborts when there is no path

			/*just for printing purposes, retrieve the result in the queue*/
			int pathSize = pathVertices.size();
			for (int i = 0; i < pathSize;i++) {
				cout << m_vertices[pathVertices.front()].GetNode() << " | ";
				pathVertices.pop_front();
			}
			cout << endl;
			return true;
		}
		return false;		//Returns false if it wasn't able to find a path

	}

	//Returns the index of the next unvisited vertex
	int GetNextVertex(int index, vector<char> &visitVertices) {
		for (int nextIndex = 0; nextIndex < m_vertices.size(); nextIndex++)		//checks in the adjacency matrix if the vertex from row "index" has an unvisited vertex. It returns the first it finds
		{
			if (m_adjMatrix[index][nextIndex] == '1' &&			//If the vertex i is adjacent to vertex index
				visitVertices[nextIndex]!='1')					//AND the vertex has not been visited
				return nextIndex;
		}
		return -1;												//Return -1 if there are no more vertices to visit
	}

	//Breadth first search using a deque.
	bool BreadthFirstSearch(int startIndex, int endIndex) {
		int currentIndex = startIndex;
		deque<int> verticesToCheck;						//Double ended queue with the vertices to check
		deque<int> pathVertices;						//Doble ended queue which holds the path from the start of the search to the end
		vector<char> visitVertices(m_vertices.size());	//vector that holds a char indicating if the vertex of the associated index was visited or not

		verticesToCheck.push_back(currentIndex);			//Push the first item at the end of the queue (as a stack) to start from there
		visitVertices[currentIndex] = '1';				//The start vertex is marked as checked


		while (!verticesToCheck.empty()) {
			currentIndex = verticesToCheck.front();
			verticesToCheck.pop_front();
			pathVertices.push_back(currentIndex);

			if (currentIndex == endIndex) {


				/*JUST FOR PRINTING*/
				for (int i = 0; !pathVertices.empty();i++) {
					cout << m_vertices[pathVertices.front()].GetNode() << " | ";
					pathVertices.pop_front();
				}
				cout << endl;
				/*END PRINT AREA*/
				return true;

			}
						
			for (int index = 0; index < m_vertices.size(); index++) {				//For all the vertices. We COULD reuse getunvisitedvertex, as Sherrod does. NEVERTHELESS, it is not efficient, as he always starts the for loop from the beginning
				if (m_adjMatrix[currentIndex][index] == '1' && visitVertices[index] != '1') {		//If the vertex is adjacent to the current vertex and it was not visited
					verticesToCheck.push_back(index);													//Push the vertex at the back of the queue
					visitVertices[index] = '1';														//Mark the vertex as checked
				}
			}

		}
		return false;
	}


	/***********************************************************
	
	Pathfinding Algorithms
	
	***********************************************************/

	/*Data structures
	
	*Open set: stores the nodes that have to be considered for the search, ordered by the lowest h(x). 
	The one with the lowest h(x) will be visited next
	Use a binary heap or priority queue ordered by the lowest h(x)
	
	*Closed set: stores the nodes that have already been evaluated and are not a possibility.
	A common operation will be to search if a vertex is in the closed set.
	Use a binary search tree to increase search performance
	
	//Look for the nodes adjacent to the current node and add them to the open set ordered by h(x)
		//If the node is in the closed set, it will not be placed in the open set
		//For the nodes added to the open set, set the current node as their parent
		//If the node is not already in the open set, calculate h(x) and add it to the open set
		//If the open set has no nodes left, there is no path
		//If the open set has nodes, obtain the node with the lowest h(x) in the openset and move it to the closed set
		//This node will be the current node
		//If the current node is the end node, the path was found
		//Traverse from the end node through its parent until reaching the start node: this is a linked list with the root being the end

	*/
	
	void SetNodePosition(int nodeIndex, Vector3 position) {
		m_vertices[nodeIndex].SetPosition(position.x, position.y, position.z);
	}


	//If a < b must return false ALWAYS
	static bool compareNodes(GraphVertex<T>* node_a, GraphVertex<T>* node_b) {
		if (node_a->GetH() > node_b->GetH())
			return true;					//node2 goes up the heap
		else
			return false;					//node1 goes up the heap
	}


	void GreedyBestFirstSearch(int startIndex, int endIndex) {

		vector<GraphVertex<T>*> openSet;								//Open set vector which will be turned into a heap to always keep the one with the lowest h(x) at the front
																		//should contain pointers to graph vertices so that it can compare their H value and use references to the vertices, not copy them
		BinarySearchTree<GraphVertex<T>*> closedSet(compareNodesOrder);	//Closed set implemented as a binary search tree to improve the search performance

		GraphVertex<T>* currentNode = &m_vertices[startIndex];			//The current node will be the one associated to the start index

		GraphVertex<T>* endNode = &m_vertices[endIndex];

		closedSet.Insert(currentNode);	//The start will be the current index and it is added to the closed set, it has been visited

		//The search will be repeated until the current node is the end node
		while (currentNode != endNode) {

			//Check all the nodes in the adjacency matrix
			for (int i = 0; i < m_vertices.size(); i++)
			{
				if (AreAdjacent(currentNode, &m_vertices[i])) {						//If the nodes are adjacent (this is not a digraph)
					if (closedSet.Search(&m_vertices[i])) {							//If the node is in the closed set, continue with the following node
						continue;
					}		
					
					m_vertices[i].SetParent(currentNode);							//The adjacent node's parent will be the current node, because the algorithm supposes it is already taking the best path
								
					//If the node is not in the closed set, add the node to the open set
					if (!IsNodeInOpenSet(&m_vertices[i], openSet)) {					//Only add the node to the open set if the open set does not have it
						
						AddNodeToOpenSet(&m_vertices[i], endNode, openSet);
						
					}
				
				}
			}
			if (openSet.empty()) {		//If the open set has no nodes left, then there is no path and you must break out of the loop
				break;
			}
			else {
				currentNode = openSet[0];								//The current node will now be the first in the heap (the one with lowest h(x)
				
				pop_heap(openSet.begin(), openSet.end(), compareNodes/*,NodeCompare<T>()*/);	//Push heap: moves the first element to the back of the heap
				
				openSet.pop_back();										//You must pop back from the heap to pop the element from the open set
				
				closedSet.Insert(currentNode);						//Always add the current index to the closed set, because it will be visited
			}

		}

		if (currentNode = endNode) {								//If the end was reached, then the final path must be built
			BuildFinalPath(endNode, "Greedy Best-First");
		}

	}

	//If a < b must return false ALWAYS
	static bool compareNodesAStar(GraphVertex<T>* node_a, GraphVertex<T>* node_b) {
		if (node_a->GetF() > node_b->GetF())
			return true;					//node2 goes up the heap
		else
			return false;					//node1 goes up the heap
	}

	void AStarSearch(int startIndex, int endIndex) {

		float newGValue;												//Float variable which will store the G value calculated

		vector<GraphVertex<T>*> openSet;								//Open set vector which will be turned into a heap to always keep the one with the lowest h(x) at the front
																		//should contain pointers to graph vertices so that it can compare their H value and use references to the vertices, not copy them
		BinarySearchTree<GraphVertex<T>*> closedSet(compareNodesOrder);	//Closed set implemented as a binary search tree to improve the search performance

		GraphVertex<T>* currentNode = &m_vertices[startIndex];			//The current node will be the one associated to the start index

		GraphVertex<T>* endNode = &m_vertices[endIndex];

		closedSet.Insert(currentNode);	//The start will be the current index and it is added to the closed set, it has been visited

		//The search will be repeated until the current node is the end node
		while (currentNode != endNode) {

			//Check all the nodes in the adjacency matrix
			for (int i = 0; i < m_vertices.size(); i++)
			{
				if (AreAdjacent(currentNode, &m_vertices[i])) {							//If the nodes are adjacent (this is not a digraph)
					if (closedSet.Search(&m_vertices[i])) {								//If the node is in the closed set, continue with the following node
						continue;
					}																	//If the node is not in the closed set, add the node to the open set					
					
					if (IsNodeInOpenSet(&m_vertices[i], openSet)) {						//If the node is in the open set, then the g value must be calculated for the new parent				
						
						newGValue = m_vertices[i].CalculateGForParent(currentNode);
						
						if (newGValue < m_vertices[i].GetG()) {							//If g(currentNode) < g(previous parent), then the current node must be the parent
							
							m_vertices[i].SetParent(currentNode);						//The current node will be the parent

							m_vertices[i].SetG(newGValue);								//g(x) will be the new G value of the new parent

							m_vertices[i].CalculateF();									//Recalculate f because g has been updated

						}
						
					}
					else {																//If the node was not in the open set

						m_vertices[i].SetParent(currentNode);							//The current node will be the parent

						AddNodeToOpenSetAStar(&m_vertices[i], endNode, openSet);		//Only add the node to the open set if the open set does not have it

					}

				}
			}
			if (openSet.empty()) {		//If the open set has no nodes left, then there is no path and you must break out of the loop
				break;
			}
			else {
				currentNode = openSet[0];								//The current node will now be the first in the heap (the one with lowest h(x)

				pop_heap(openSet.begin(), openSet.end(), compareNodesAStar);	//Push heap: moves the first element to the back of the heap

				openSet.pop_back();										//You must pop back from the heap to pop the element from the open set

				closedSet.Insert(currentNode);						//Always add the current index to the closed set, because it will be visited
			}

		}

		if (currentNode = endNode) {								//If the end was reached, then the final path must be built
			BuildFinalPath(endNode, "A Star");
		}

	}



	vector<GraphVertex<T>> m_vertices;		//A vector of graph vertices
	char** m_adjMatrix;						//Adjacency matrix will be a dynamically created 2 dimensional matrix with chars

private:
	//vector<GraphVertex<T>> m_vertices;		//A vector of graph vertices
	int m_maxVerts;							//The size of the graph must be fixed
	//char** m_adjMatrix;						//Adjacency matrix will be a dynamically created 2 dimensional matrix with chars

	//Returns true if node1 has an edge to node2, first obtaining their corresponding indexes
	bool AreAdjacent(GraphVertex<T>* node1, GraphVertex<T>* node2) {
		return (m_adjMatrix[node1->GetIndex()][node2->GetIndex()] == '1');
	}

	//Adds a node to open set and sets the parent
	void AddNodeToOpenSet(GraphVertex<T>* currentNode, GraphVertex<T>* endNode, vector<GraphVertex<T>*> &openSet) {

		currentNode->CalculateH(endNode->GetPosition());					//Calculate h(x) for the index that is going to be inserted

		openSet.push_back(currentNode);									//Insert the node index in the open set

		push_heap(openSet.begin(), openSet.end(), compareNodes/*NodeCompare<T>()*/);	//Push the item as a heap in the vector

	}

	//Adds a node to open set and sets the parent for A*
	void AddNodeToOpenSetAStar(GraphVertex<T>* currentNode, GraphVertex<T>* endNode, vector<GraphVertex<T>*>& openSet) {

		currentNode->CalculateG();

		currentNode->CalculateH(endNode->GetPosition());			//Calculate h(x) for the index that is going to be inserted

		currentNode->CalculateF();									//Calculate f(x) now that g and h are updated

		openSet.push_back(currentNode);								//Insert the node index in the open set

		push_heap(openSet.begin(), openSet.end(), compareNodesAStar);	//Push the item as a heap in the vector

	}

	//Function to compare values for the BST of the closed set
	//Converted to static to avoid the error "non-standard syntax; use & to create a pointer to member"
	static int compareNodesOrder(GraphVertex<T>* node_a, GraphVertex<T>* node_b) {
		return(node_a->GetIndex() - node_b->GetIndex());
	}

	//Iterates through the vector/heap and returns true if the index is found
	bool IsNodeInOpenSet(GraphVertex<T>* node, vector<GraphVertex<T>*>& vector) {
		for (int i = 0; i < vector.size();i++) {
			if (vector[i] == node) return true;
		}
		return false;
	}

	//Builds the final path for pathfinding algorithms, given an inverted list of nodes starting from the end node 
	void BuildFinalPath(GraphVertex<T>* endNode, string algorithmName) {

		stack<GraphVertex<T>*> finalPath;

		GraphVertex<T>* currentNode = endNode;

		while (currentNode != nullptr) {
			finalPath.push(currentNode);
			currentNode = currentNode->GetParent();
		}
		cout << "Path result for algorithm: " << algorithmName <<endl;
		for (int i = 0; !finalPath.empty();i++) {
			cout << finalPath.top()->GetNode() << " | ";
			finalPath.pop();
		}
		cout << endl;

	}

};










