#pragma once

template<class T>
class OrderedArray
{

public:

	//Constructor
	OrderedArray(int size, int growBy = 1);

	//Destructor
	~OrderedArray();

	//Insert an item at the end of the array
	virtual void push(T item);

	//Remove the last item
	virtual void pop();

	//Remove an item at a certain position
	virtual void remove(int index);

	//Clear the whole array
	virtual void clear();

	//It will be a binary search. Returns -1 if it didn't find the item
	int searchItem(T item);

	int numElements;	//Current number of elements

	int maxSize;	//Maximum size allowed by the array

	virtual T& operator[](int index);

private:
		
	T* uArray;			//pointer to a T* array

	int growSize;		//Growth of the array if expanded

	bool expand();		//Used to expand the array in case the space is not enough

};


template <class T>
OrderedArray<T>::OrderedArray(int size, int growBy) :
	uArray(nullptr), maxSize(0), growSize(0), numElements(0)
{

	if (size > 0) {
		maxSize = size;
		growSize = (growBy > 0 ? growBy : 0);
		uArray = new T[maxSize];
		numElements = 0;
	}
}

template <class T>
OrderedArray<T>::~OrderedArray() {
	if (uArray != nullptr) {
		delete uArray;
		uArray = nullptr;
	}
}

template <class T>
void OrderedArray<T>::push(T item) {
	if (uArray != nullptr) {
		if (numElements >= maxSize) {
			expand();
		}
		
		//Search the place where the item must be located
		int currentIndex = 0;
		while (item > uArray[currentIndex] && currentIndex <=numElements-1) {
			currentIndex++;
		}

		//Once found, move all the objects from that position to the next position, starting from the back
		for (int i = numElements; i > currentIndex;i--) {
			uArray[i] = uArray[i-1];
		}

		//Assign the item to the current index
		uArray[currentIndex] = item;

		//increase the number of elements
		numElements++;

	}
}

template<class T>
inline void OrderedArray<T>::pop()
{
	if (numElements > 0) {
		numElements--;
	}
}


/*Removes an index, regardless of it having or not a value*/
template<class T>
inline void OrderedArray<T>::remove(int index) {

	//Only execute it if the element exists, that is, if the index is less than numElements (I can't get to work that (uArray + index)!=nullptr
	if (index >= 0 && index <= maxSize) {

		//This loop will overwrite each element with the following one starting from the index.
		//Up to the number of elements minus 1 because the last element will not be overwritten
		//If the element is the last in the list, that is index+1 == numElements => the loop will not execute
		for (int i = index; i < (maxSize - 1); i++) {
			uArray[i] = uArray[i + 1];
		}

		maxSize--;		//It has to reduce the number of elements because it removed an element, so the array will not get to that element

		if (numElements > maxSize) {
			numElements = maxSize;
		}
	}
}

template<class T>
inline void OrderedArray<T>::clear() {
	numElements = 0;
}

template<class T>
bool OrderedArray<T>::expand() {
	//Will only grow if the growth size is bigger than 0
	if (growSize > 0) {
		T* newArray = new T[maxSize + growSize];	//creates a new array with the current maxsize + the growth size

		memcpy(newArray, uArray, sizeof(T) * maxSize);

		delete[] uArray;		//deletes the previously allocated memory. 
		//delete uArray also works BUT it's incorrect, because it doesn't deallocate every element of the array, only the first element

		maxSize += growSize;	//now the max size is the maxsize+growth size

		uArray = newArray;		//the field pointer will now have the new array

		return true;
	}
	else
		return false;
}

template<class T>
T& OrderedArray<T>::operator[](int index)
{
	assert(uArray != nullptr && index <= numElements);
	
	return uArray[index];
}

template<class T>
int OrderedArray<T>::searchItem(T item) {

	int midPos, startPos, endPos;
	midPos = (int) numElements / 2;
	startPos = 0;
	endPos = numElements - 1;

	while (uArray[midPos] != item && startPos < endPos) {
		if (item < uArray[midPos]) {
			endPos = midPos - 1;
		}
		else {
			startPos = midPos + 1;
		}
		midPos = (int)(endPos - startPos) / 2 + startPos;
	}

	if (uArray[midPos] == item) {
		return midPos;
	}
	else {
		return -1;
	}





}