#pragma once
#include<list>
//#include "TreeIterator.h"

//Based on Penton

//template<class DataType> class TreeIterator;

using namespace std;

template<class DataType>
class Tree
{
	//friend class TreeIterator<DataType>;
	
public:
	
	Tree(DataType data): m_data(data) {
		m_parent = nullptr;
		m_data = data;
	}

	//Destroys the tree
	~Tree() {
		Destroy();
	}

	//Recursively destroys the tree node and all of its children
	void Destroy() {
		typedef list<Tree<DataType>*>::iterator IteratorType;

		Tree<DataType>* nodeToDelete = nullptr;		//Store the node to be deleted
		IteratorType it = m_children.begin();		//Initialize the iterator
		IteratorType auxIt;							//Auxiliary iterator

		//While the iterator hasn't passed the end of the list
		while (it != m_children.end()) {
			nodeToDelete = *it;					//The iterator will always return a pointer. In this case, a pointer to the item of a list which is a pointer
			auxIt = it;							//We need to store it because if I perform erase(it), when it++ it will return an error because the iterator is invalid
			it++;								//The iterator must increase because the previous position will be erased
			m_children.erase(auxIt);				//After deleting the node's children, erase the node from the list
			delete nodeToDelete;				//when you delete the node it will recursively delete the children
		}

	}

	//Count the numberof nodes in the tree
	int Count() {
		int numNodes = 1;
		typedef list<Tree<DataType>*>::iterator IteratorType;
		for (IteratorType it = m_children.begin(); it != m_children.end(); it++) {
			numNodes = numNodes + it->Count();
		}
		return numNodes;

	}

	//Adds a node as a child
	void AddChild(Tree<DataType>* treeNode) {
		m_children.push_back(treeNode);				//Pushes the child to the end of the list
		treeNode->m_parent = this;					//Adds itself as a parent of the child
	}

	Tree<DataType>* GetParent() {
		return m_parent;
	}

	DataType& GetData() {
		return m_data;
	}

	list< Tree<DataType>* >& GetChildren() {
		return m_children;
	}

	//Processes the elements in preorder
	/*void Preorder(void (*p_process)(Tree<DataType>*)) {
	*    typedef list<Tree<DataType>*>::iterator IteratorType;
	* 
		p_process(this);
		TreeIterator<DataType> it(this);

	}*/

	//list< Tree<DataType>* > m_children;	//list containing pointers to trees

private:
	
	Tree<DataType>* m_parent;	//pointer to parent node
	DataType m_data;
	list< Tree<DataType>* > m_children;	//list containing pointers to trees
	//Tree<DataType>* m_sibling;	//Pointer to sibling node of the tree is not necessary because this is maintained by the list

};

