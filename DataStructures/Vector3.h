#pragma once


class Vector3
{
public:
	float x, y, z;		//Vector coordinates

	Vector3(float newX, float newY, float newZ): x(newX), y(newY), z(newZ) {	}
	
	~Vector3() {}

	Vector3 operator+(Vector3 otherVec);


};

