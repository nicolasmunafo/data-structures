#pragma once
#include "Vector3.h"
#include <cmath>

template<class T>
class GraphVertex
{
public:
	GraphVertex(T newNode, int nodeIndex): 
		m_node(newNode), m_position(Vector3(0,0,0)), 
		m_parent(nullptr), m_h(-1), m_f(-1), m_g(-1), m_nodeCost(1),
		m_nodeIndex(nodeIndex) {

	}

	~GraphVertex()
	{

	}

	T GetNode() {
		return m_node;
	}

	/*For pathfinding purposes*/
	void SetPosition(float x, float y, float z) {
		m_position.x = x;
		m_position.y = y;
		m_position.z = z;
	}


	
	//Set the parent of the graph as the new parent to backtrack in order to get the final path
	void SetParent(GraphVertex<T>* newParent) {
		m_parent = newParent;
	}



	/*Calculate H, heuristics, using Manhattan distance
	*   h(x) = |start.x - end.x | + |start.y - end.y|
	*/
	void CalculateH(Vector3 destination) {
		m_h = abs(destination.x - m_position.x) +
			abs(destination.y - m_position.y) +
			abs(destination.z - m_position.z);
	}

	/*Calculate g, which is the cost from the start node to the current node.
	If the node does not have a parent node, its cost is 1
	If the node has a parent node, it will obtain the cost from its parent node, which may recursively obtain it or return 1
	*/
	void CalculateG() {
		if (m_parent != nullptr) {
			m_g = m_parent->GetG();
		}
		else {
			m_g = m_nodeCost;
		}
	}

	float CalculateGForParent(GraphVertex<T>* newParent) {
		return (m_nodeCost + newParent->GetG());
	}

	//Only used to set the value of g when the G value is calculated with "CalculateGForParent", not to calculate it twice
	void SetG(float gValue) {
		m_g = gValue;
	}
	
	//Calculate f as the sum of g + h (the cost from the start node and the heuristics calculation)
	void CalculateF() {
		m_f = m_g + m_h;
	}

	/************************
	
	Getters
	
	*************************/

	float GetF() {
		return m_f;
	}

	int GetIndex() {
		return m_nodeIndex;
	}

	float GetH() {
		return m_h;
	}

	float GetG() {
		return m_g;
	}

	Vector3 GetPosition() {
		return m_position;
	}

	GraphVertex<T>* GetParent() {
		return m_parent;
	}

	bool operator==(GraphVertex<T> otherVertex) {
		return (m_node == otherVertex.m_node);
	}

	/*End of pathfinding area*/

private:
	T m_node;
	int m_nodeIndex;				//Stores the node index in the Graph Vertices vector to improve searches

	/*For pathfinding*/
	Vector3 m_position;				//Position to calculate the heuristics
	GraphVertex<T>* m_parent;		//Parent of the current node to track which node was visited before this one
	float m_h;						//Heuristics calculated for a specific pathfinding search. Initiated with -1
	float m_g;						//Path-cost from the start node to the current node of a specific pathfinding algorithm A*
	float m_f;						//f = g+h for a specific pathfinding algorithm
	float m_nodeCost;				//Node cost for calculating g(x)

};

