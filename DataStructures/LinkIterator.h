#pragma once

#include "LinkNode.h"
#include <assert.h>

//An iterator points to an element within the list
//Basically has a single attribute which is a pointer to a node T
//The methods are the same as a pointer has: assignment, increment, comparison

template<class T>
class LinkIterator
{
	//Link list must access the node of the iterator, otherwise it will not be able to insert after or before
	friend class LinkList<T>;
public:
	LinkIterator() : ptrNode(nullptr) {
	}

	~LinkIterator();

	//Assigns a node to the ptrNode of the iterator
	void operator= (LinkNode<T> *newNode);
	
	//Dereference operator: returns as a type the address of a specific type
	T &operator* ();

	//obtains the next node of the list, using a prefix operator
	void operator++();

	//obtains the next node of the list, using a postfix operator
	void operator++(int);

	//obtains the previous node of the list, using a prefix operator
	void operator--();

	//obtains the previous node of the list, using a postfix operator
	void operator--(int);

	bool operator!=(LinkNode<T> *node);

	bool operator==(LinkNode<T>* node);

	//Checks if the iterator is valid, added for priority queues
	bool IsValid() {
		return (ptrNode != nullptr);
	}

private:

	//The pointer to the current node is a pointer to a node of type T
	LinkNode<T> *ptrNode;
};

//The destructor should not delete the memory allocated by another data structure
//"delete ptrNode" - Was here, but if the destructor is called in the original node after this destructor
//it will try to delete nullptr and everything will explode
template<class T>
LinkIterator<T>::~LinkIterator() {

}

template<class T>
inline void LinkIterator<T>::operator=(LinkNode<T> *newNode)
{
	ptrNode = newNode;
}

template<class T>
T &LinkIterator<T>::operator*() {

	assert(ptrNode != nullptr);
	return ptrNode->nodeData;
}

template<class T>
inline void LinkIterator<T>::operator++() {
	assert(ptrNode != nullptr);
	ptrNode = ptrNode->ptrNext;
}

template<class T>
inline void LinkIterator<T>::operator++(int) {
	assert(ptrNode != nullptr);
	ptrNode = ptrNode->ptrNext;
}

template<class T>
inline void LinkIterator<T>::operator--() {
	assert(ptrNode != nullptr);
	ptrNode = ptrNode->ptrPrev;
}

template<class T>
inline void LinkIterator<T>::operator--(int) {
	assert(ptrNode != nullptr);
	ptrNode = ptrNode->ptrPrev;
}

template<class T>
inline bool LinkIterator<T>::operator!=(LinkNode<T>* node) {
	return (ptrNode != node);
}

template<class T>
inline bool LinkIterator<T>::operator==(LinkNode<T>* node) {
	return (ptrNode == node);
}
