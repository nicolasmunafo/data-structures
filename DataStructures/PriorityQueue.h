#pragma once
#include "LinkList.h"
#include "LinkIterator.h"
#include <assert.h>

//The template has two items:
// T - data type used by the queue
// CMP - Comparison object that will have an operator used to compare the data. This is to avoid hardcoding the comparison operator
template<typename T, typename CMP>
class PriorityQueue
{
public:
	//Constructor
	PriorityQueue(int size) {
		assert(size > 0);
		m_size = size;
	}


	//Destructor
	~PriorityQueue() {}

	//When elements are inserted at the back of the list use PushBack and PopBack
	//inserts an item at the front of the queue (back of the list)
	void Push(T item) {
		assert(m_elements.GetLength() < m_size);

		//If there are no elements in the queue, it will push it as usual
		if (m_elements.GetLength() == 0) {
			m_elements.Push(item);
		}
		else {	//If there are elements, it must compare them to place them according to the priority
			CMP cmp;		//create an object of type cmp used to compare a pair of data

			LinkIterator<T> it;
			it = m_elements.GetRoot();			//Create an iterator starting from the root of the associated list

			//traverse through the queue while the comparison doesn't return true
			//and while the iterator is valid
			//The comparison object has an operator () which compares the item of type T and the data of *it (also of type T)
			while (it.IsValid()) {
				if (cmp(item, *it)) {
					break;				//I added both comparisons inside the while, but it caused an error because *it was null
				}
				it++;
			}

			//If the iterator is valid, then it didn't reach the end of the list
			//It must insert the item before the iterator position
			if (it.IsValid()) {
				m_elements.InsertBefore(it, item);
			}
			else {//If the iterator is not valid, it will insert the item at the end of the list
				m_elements.Push(item);
			}
		}

	}

	//removes an item from the front of the queue
	void Pop() {
		m_elements.PopFront();
	}

	//Retrieves the node from the front of the queue. 
	//As elements are inserted at the back, the Root is the front
	T& Front() {
		LinkIterator<T> it;
		it = m_elements.GetRoot();
		return *it;
	}

	//Retrieves the node from the back of the queue. 
	//As elements are inserted at the back, the Last is the back
	T& Back() {
		LinkIterator<T> it;
		it = m_elements.GetLast();
		return *it;
	}


	//Returns the size of the queue
	int GetSize() {
		return m_elements.GetLength();
	}

	int GetMaxSize() {
		return m_size;
	}

	bool IsEmpty() {
		return m_elements.GetLength() == 0;
	}

	void Resize(int newSize) {
		assert(newSize > 0);
		m_size = newSize;
	}

private:
	LinkList<T> m_elements;
	int m_size;
};

