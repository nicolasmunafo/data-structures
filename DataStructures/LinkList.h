#pragma once

#include "LinkNode.h"

/*Link List: 

ROOT -> Node A -> Node B  <- LAST

Inserts and removals are only through the LAST pointer

_________________________________________________________________

*Double-ended list: same, but inserts and removals can be through the ROOT pointer

_________________________________________________________________
*Doubly-Linked lists: Root -> Node A <=> Node B <=> Node C <- Last

Every node has a pointer to the next and the previous node (the first node's previous and the last node's next are null


*/

template <class T>
class LinkList
{

public:
	
	LinkList() : ptrRoot(nullptr), ptrLast(nullptr), numElements(0) {
	}

	~LinkList();

	//Inserts an item at the end of the list
	void Push(T item);

	//Inserts an item at the front of the list, for double-ended link list
	void PushFront(T item);

	//Removes the last item in the list
	void Pop();

	//Inserts an item at the beginning of the list, for double-ended link list
	void PopFront();

	LinkNode<T>* GetRoot();

	LinkNode<T>* GetLast();

	int GetLength();

	//For priority queues, this lets you insert an item after a specific iterator
	void InsertAfter(LinkIterator<T> &it, T item);

	//For priority queues, this lets you insert an item before a specific iterator
	void InsertBefore(LinkIterator<T>& it, T item);

private:
	//Pointer to the root of the list
	LinkNode<T>* ptrRoot;

	//Pointer to last node
	LinkNode<T>* ptrLast;

	//Stores the amount of elements
	int numElements = 0;
};

template<class T>
LinkList<T>::~LinkList() {
	while (ptrRoot != nullptr) {
		Pop();
	}
}

template<class T>
LinkNode<T>* LinkList<T>::GetRoot() {
	return ptrRoot;
}

template<class T>
LinkNode<T>* LinkList<T>::GetLast() {
	return ptrLast;
}

template<class T>
int LinkList<T>::GetLength() {
	return numElements;
}

template<class T>
void LinkList<T>::Push(T item) {
	//Create a new node and assign the item data
	LinkNode<T>* newNode = new LinkNode<T>;
	newNode->nodeData = item;
	newNode->ptrNext = nullptr;
	newNode->ptrPrev = nullptr;

	//If there is more than one element, assign it to the end of the list
	if (ptrLast!=nullptr) {

		//The pointer of the last node must point to the new node. 
		//If we didn't have the ptrLast we would have to traverse through the whole list to obtain the last node
		ptrLast->ptrNext = newNode;
		//The new node will point to the node which was previously the last one
		newNode->ptrPrev = ptrLast;
		//Now the new point is the last point
		ptrLast = newNode;
	}
	else {
		//Both the root and the last node will point to the new node
		ptrRoot = newNode;
		ptrLast = newNode;
	}

	//Increment number of elements
	numElements++;
}



template<class T>
void LinkList<T>::Pop() {
	if (ptrRoot != nullptr) {
		//If the last node doesn't point to the beginning

		if (ptrRoot->ptrNext == nullptr) {
			delete ptrRoot;
			ptrRoot = nullptr;
		}
		else {
			//It is not necessary to traverse through the list because it is a doubly-linked list
			//we can obtain the node before the last one with the previous pointer
			//store the last node in an auxiliary one
			
			LinkNode<T>* auxNode = ptrLast;
			//The pointer to the end will now point to the previous node
			ptrLast = ptrLast->ptrPrev;
			//The node that is now the last one must point to NULL
			ptrLast->ptrNext = nullptr;
			//delete the auxiliary node
			delete auxNode;
			auxNode = nullptr;						
		}
		//Decrement the number of elements
		numElements--;
	}
}



template<class T>
void LinkList<T>::PushFront(T item) {
	//Create a new node and assign the item data
	LinkNode<T>* newNode = new LinkNode<T>;
	newNode->nodeData = item;

	if (ptrRoot != nullptr) {
		//the next node will be the node pointed by the root
		newNode->ptrNext = ptrRoot;

		//Added for doubly-linked list
		ptrRoot->ptrPrev = newNode;

		//Now the root will point to the new node
		ptrRoot = newNode;

		
	}
	else {
		ptrRoot = newNode;
		ptrLast = newNode;
	}
	//Increment number of elements
	numElements++;
}

template<class T>
void LinkList<T>::PopFront() {

	assert(ptrRoot != nullptr);
	//Store the current node to be deleted
	LinkNode<T>* auxNode = ptrRoot;

	//The root will now point to the second node
	ptrRoot = ptrRoot->ptrNext;

	if (ptrRoot != nullptr) {
		//Added for doubly linked lists: the new first node will not have a previous node
		ptrRoot->ptrPrev = nullptr;
	}

	delete auxNode;
	auxNode = nullptr;

	//Decrement the number of elements
	numElements--;

}

template<class T>
void LinkList<T>::InsertAfter(LinkIterator<T>& it, T item) {
	assert(it.IsValid());		//check if the iterator is valid
	LinkNode<T>* newNode = new LinkNode<T>();			//create a new node
	assert(newNode != nullptr);
	newNode->nodeData = item;							//Store the item in the data of the node
	LinkNode<T>* currentNode = it.ptrNode;						//store the current node pointed by the iterator

	//if the current node A has a node B after it, the new node must be placed before the B node 
	if (currentNode->ptrNext != nullptr) {
		currentNode->ptrNext->ptrPrev = newNode;			//the previous pointer of the node after the current node will now point to the new node
	}
	newNode->ptrNext = currentNode->ptrNext;			//the next pointer of the new node will be the B node
	currentNode->ptrNext = newNode;						//The current node will point now to the new node
	newNode->ptrPrev = currentNode;						//The new node will point to the node pointed by the iterator

	//If the Last pointer pointed at the current node, now the last pointer must point to the new node
	if (ptrLast == currentNode) {
		ptrLast = newNode;
	}

	//increment the number of elements
	numElements++;
}

template<class T>
void LinkList<T>::InsertBefore(LinkIterator<T>& it, T item) {
	assert(it.IsValid());		//check if the iterator is valid
	LinkNode<T>* newNode = new LinkNode<T>();			//create a new node
	assert(newNode != nullptr);
	newNode->nodeData = item;							//Store the item in the data of the node
	LinkNode<T>* currentNode = it.ptrNode;				//store the current node pointed by the iterator

	//if the current node A has a node B before it, the new node must be placed after the B node 
	if (currentNode->ptrPrev != nullptr) {
		currentNode->ptrPrev->ptrNext = newNode;		//the next pointer of the node before the current node will now point to the new node
	}
	newNode->ptrPrev = currentNode->ptrPrev;			//the previous pointer of the new node will point to the B node
	currentNode->ptrPrev = newNode;						//The prev pointer of the current node will point now to the new node
	newNode->ptrNext = currentNode;						//The next ptr of the new node will point to the node pointed by the iterator

	//If the Root pointer pointed at the current node, now the root pointer must point to the new node
	if (ptrRoot == currentNode) {
		ptrRoot = newNode;
	}

	//increment the number of elements
	numElements++;
}






/*
template<class T>
void LinkList<T>::Push(T item) {
	//Create a new node and assign the item data and the next will be nullptr
	LinkNode<T>* newNode = new LinkNode<T>;
	newNode->nodeData = item;
	newNode->ptrNext = nullptr;

	//If there are more than one element, assign it to the end of the list
	if (numElements > 0) {

		//The pointer of the last node must point to the new node.
		//If we didn't have the ptrLast we would have to traverse through the whole list to obtain the last node
		ptrLast->ptrNext = newNode;
		//Now the new point is the last point
		ptrLast = newNode;
	}
	else {
		//Both the root and the last node will point to the new node
		ptrRoot = newNode;
		ptrLast = newNode;
	}

	//Increment number of elements
	numElements++;
}

template<class T>
void LinkList<T>::Pop() {
	if (ptrLast != nullptr) {
		//If the last node doesn't point to the beginning
		if (ptrLast != ptrRoot) {
			LinkNode<T>* currentNode = ptrRoot;

			//Iterate through the list will the pointer of the current node doesn�t point to the last node
			while (currentNode->ptrNext != ptrLast) {
				currentNode = currentNode->ptrNext;
			}

			//The current node is pointing to the first to last node (the one before the last)
			delete ptrLast;
			currentNode->ptrNext = nullptr;
			ptrLast = currentNode;
		}
		else {
			delete ptrLast;
			ptrRoot = nullptr;
		}

		//Decrement the number of elements
		numElements--;
	}
}



template<class T>
void LinkList<T>::PushFront(T item) {
	//Create a new node and assign the item data
	LinkNode<T>* newNode = new LinkNode<T>;
	newNode->nodeData = item;
	newNode->ptrNext = nullptr;

	if (ptrRoot != nullptr) {
		//the next node will be the node pointed by the root
		newNode->ptrNext = ptrRoot;

		//Now the root will point to the new node
		ptrRoot = newNode;
	}
	else {
		ptrRoot = newNode;
		ptrLast = newNode;
	}
	//Increment number of elements
	numElements++;
}

template<class T>
void LinkList<T>::PopFront() {

	if (ptrRoot != nullptr) {
		//Store the current node to be deleted
		LinkNode<T>* currentNode = ptrRoot;

		//The root will now point to the second node
		ptrRoot = ptrRoot->ptrNext;

		delete currentNode;
	}
	//Decrement the number of elements
	numElements--;

}


*/

