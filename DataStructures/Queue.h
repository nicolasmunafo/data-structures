#pragma once
#include "LinkList.h"
#include "LinkIterator.h"
#include <assert.h>

template<class T>
class Queue
{
public:
	//Constructor
	Queue(int size) {
		assert(size > 0);
		m_size = size;
	}

	
	//Destructor
	~Queue() {}

	//When elements are inserted at the back of the list use PushBack and PopBack
	//inserts an item at the front of the queue (back of the list)
	void PushBack(T item) {
		if (m_elements.GetLength() < m_size) {
			m_elements.Push(item);
		}

	}

	//removes an item from the front of the queue
	void PopBack() {
		m_elements.PopFront();
	}

	//Retrieves the node from the front of the queue, when elements are inserted at the back
	//In this case, the back of the queue is the root of the list
	T& Back() {
		LinkIterator<T> it;
		it = m_elements.GetRoot();
		return *it;
	}



	//When elements are inserted at the front call PushFront and PopFront
	//inserts an item at the front of the queue (front of the list)
	void PushFront(T item) {
		if (m_elements.GetLength() < m_size) {
			m_elements.PushFront(item);
		}
	}

	//Removes an item at the back of the queue (back of the list)
	void PopFront() {
		m_elements.Pop();
	}

	//Retrieves the node from the front of the queue when elements are inserted at the front of the list
	//In this case, it's the last node of the list
	T& Front() {
		//The only way to retrieve an element from the list is with an iterator because the node is private
		LinkIterator<T> it;
		it = m_elements.GetLast();
		return *it;
	}

	//Returns the size of the queue
	int GetSize() {
		return m_elements.GetLength();
	}

	int GetMaxSize() {
		return m_size;
	}

	bool IsEmpty() {
		return m_elements.GetLength() == 0;
	}

	void Resize(int newSize) {
		assert(newSize > 0);
		m_size = newSize;
	}

private:
	LinkList<T> m_elements;
	int m_size;
};

