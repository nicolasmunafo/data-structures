#pragma once
#include "Tree.h"
#include <list>

template<class DataType>
class TreeIterator
{
	//friend class Tree<DataType>;									//So that the tree can access the iterator

public:
	TreeIterator(Tree<DataType> *treeNode) {
		*this = treeNode;											//Uses the assignment operator overridden for the TreeIterator
	}

	//Assign a tree node pointer to the current node
	//When you call the assignment you must assign it to a pointer or the address of a node
	void operator=(Tree<DataType>* treeNode) {
		m_currentNode = treeNode;
		ResetIterator();											//Resets the iterator of the list of children
	}



	//Moves the iterator to the root of the tree
	void Root() {
		if (m_currentNode != nullptr) {									//If the node is null, it will not have parent and the iterator should not be reset
			while (m_currentNode->GetParent() != nullptr) {
				m_currentNode = m_currentNode->GetParent();
			}
			ResetIterator();											//It should also reset the iterator to the beginning of the root's children
		}
	}

	//Move the iterator up one level. If the current node is root, it will move past the root node (it will be nullptr)
	void Up() {
		if (m_currentNode != nullptr) {
			Tree<DataType>* newCurrentNode;
			newCurrentNode = m_currentNode->GetParent();				//Assign the parent node in an auxiliary node
			if (newCurrentNode != nullptr) {							//If the parent node was not null then the iterator must be reset
				ResetIterator();
			}
			m_currentNode = newCurrentNode;								//In any case the current node must now be the parent node
		}
		
	}

	//Moves the iterator down to the node pointed by the child iterator
	void Down() {
		if (IsIteratorValid()) {					//Execute only if the iterator is valid
			*this = *m_childIterator;				//using the assignment operator, assign the child iterator
		}
	}

	//Moves the iterator in the children list to the next child
	void ChildForth() {
		m_childIterator++;
	}

	//Moves the iterator in the children list to the previous child
	void ChildBack() {
		m_childIterator--;
	}

	//Moves the iterator in the children list to the first child
	void ChildStart() {
		m_childIterator = m_currentNode->GetChildren().begin();
	}

	//Moves the iterator in the children list to the first child
	void ChildEnd() {
		m_childIterator = m_currentNode->GetChildren().end();
	}

	//Dereference operator, returns a reference to the data stored in the current pointer
	DataType& operator*() {
		return m_currentNode->GetData();
	}




private:
	Tree<DataType>* m_currentNode;									//Iterator/pointer to the current tree node
	typename list<Tree<DataType>*>::iterator m_childIterator;		//Iterator to a child of the list of pointers to the tree nodes

	//Checks if the iterator is valid, that is, if it is different to the end of the list
	bool IsIteratorValid() {
		return (m_childIterator != m_currentNode->GetChildren().end() && m_currentNode!= nullptr);
	}

	//Resets the iterator to the beginning of the list of children of the current node
	void ResetIterator() {
		if (m_currentNode != nullptr) {									//Check if the current node is not null
			m_childIterator = m_currentNode->GetChildren().begin();		//The child iterator must now point to the beginning of the list of children
		}
		/*If the current node is null, the iterator should be invalidated.
		* To invalidate an iterator you must assign the end of the list
		* However, the node is null, so there is no list and there is no way to invalidate it
		* It is important before using the iterator to check if the current node is not null
		else {															//If the current node is null, invalidate the iterator
			m_childIterator = m_currentNode->GetChildren().end();		//The only way to invalidate an iterator is to make them equal to the end of the list
		}*/
	}
};

