#pragma once
#include "UnorderedArray.h"
#include <assert.h>

template<class T>
class StackArray
{
public:
	StackArray(int size, int growBy = 1);

	~StackArray();

	void Push(T item);

	void Pop();

	T& Top();

	int GetSize();

private:
	UnorderedArray<T>* m_container;

};

//Constructor, creates the container as an UnorderedArray
template<class T>
StackArray<T>::StackArray(int size, int growBy) {
	if (size > 0 && growBy >= 0) {
		m_container = new UnorderedArray<T>(size, growBy);
		assert(m_container != nullptr);
	}	
}

//Destructor, clears the data of the UnorderedArray
template<class T>
StackArray<T>::~StackArray() {
	if (m_container != nullptr) {
		//m_container->~UnorderedArray();
		delete m_container;
		m_container = nullptr;
	}
}

template<class T>
void StackArray<T>::Push(T item) {
	m_container->push(item);
}

template<class T>
void StackArray<T>::Pop() {
	m_container->pop();
}

template<class T>
T& StackArray<T>::Top() {
	return (*m_container)[m_container->GetSize() - 1];
}

template<class T>
int StackArray<T>::GetSize() {
	assert(m_container != nullptr);
	return m_container->GetSize();
}

