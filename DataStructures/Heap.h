#pragma once
#include <vector>

using namespace std;

template<class Key>
class Heap
{
public:
	Heap() {}
	
	//Enter the minimum size of the heap array
	Heap(int minSize) {
		m_heap.reserve(minSize);			//Reserve a minimum size for the vector
	}

	~Heap() {}

	//Insert an item and walk it up until it gets to the right position
	void Push(Key data) {
		int currentPos, parentPos;
		typename vector<Key>::iterator it;
		Key auxData;

		//Insert the element at the back of the vector
		m_heap.push_back(data);

		currentPos = m_heap.size() - 1;					//Store the current position to verify the index
		auxData = m_heap[currentPos];					//Store the data here not to swap all the time when it compares parent and child

		while (currentPos != 0) {
			//If the parent is less than the current value, swap them
			parentPos = (int)(currentPos - 1) / 2;
			if (auxData > m_heap[parentPos]) {
				m_heap[currentPos] = m_heap[parentPos];
				currentPos = parentPos;
			}
			else {			//If the parent is greater than the current value, exit the loop
				break;
			}
		}
		m_heap[currentPos] = auxData;

	}

	void Pop() {

		int leftIndex, rightIndex, currentPos, nextPos, heapSize;
		Key auxData;

		if (m_heap.size() > 0) {
			//The first element of the vector is replaced with the last element
			m_heap[0] = m_heap[m_heap.size() - 1];
			//Resize the vector so that now has size-1 elements (the vector removes the ones beyond and destroys them)
			m_heap.resize(m_heap.size() - 1);
		}

		heapSize = m_heap.size();

		if (m_heap.size() > 1) {			//This is added in case the user tries to pop an item when there are no items
			//Until it finds the correct position, check which of the children of the last element is the greatest
		//Repeat until you find the correct position
			currentPos = 0;
			auxData = m_heap[currentPos];
			leftIndex = 2 * currentPos + 1;
			rightIndex = 2 * currentPos + 2;
			while (leftIndex < heapSize) {			//If the left child exceeds the size, exit the loop
				//Check which of the children is the greatest. The current node will be compared to that one
				if (rightIndex < heapSize && m_heap[leftIndex] < m_heap[rightIndex])
					nextPos = rightIndex;
				else
					nextPos = leftIndex;			//If there is no right child or the left is greater, compare to the left one
				
				//Compare the current node to the greatest child
				if (auxData > m_heap[nextPos]) {
					break;										//If the data is greater than the greatest child, exit the loop: it is already a heap
				}
				else {											//If the data is smaller than the greatest child, the child must be placed as a parent
					m_heap[currentPos] = m_heap[nextPos];		//The element must be positioned as a parent
					currentPos = nextPos;						//Now the current position will be the one of the greatest child
				}
				leftIndex = 2 * currentPos + 1;					//Obtain the children indexes of the current node
				rightIndex = 2 * currentPos + 2;
			}
			//Store the auxiliary value in the correct position
			m_heap[currentPos] = auxData;
		}

	}


	//Obtain the data from the top of the heap
	Key& Peek() {
		return m_heap[0];
	}

	//Returns the size of the heap
	int Size() {
		return m_heap.size();
	}
	vector<Key> m_heap;
private:
//	vector<Key> m_heap;

};








/*
*
*
* Pop version Sherrod
	/*
void Pop()
{
	int index = 0;
	m_heap[index] = m_heap[(int)m_heap.size() - 1];
	m_heap.pop_back();
	Key temp = m_heap[index];
	int currentIndex = 0, leftIndex = 0, rightIndex = 0;
	while (index < (int)m_heap.size() / 2)
	{
		leftIndex = 2 * index + 1;
		rightIndex = leftIndex + 1;
		if (rightIndex < (int)m_heap.size() &&
			m_heap[leftIndex] < m_heap[rightIndex])
		{
			currentIndex = rightIndex;
		}
		else
		{
			currentIndex = leftIndex;
		}
		if (temp >= m_heap[currentIndex])
			break;
		m_heap[index] = m_heap[currentIndex];
		index = currentIndex;
	}
	m_heap[index] = temp;
}

//Remove the first element of the heap structure
void Pop() {

	int leftIndex, rightIndex, currentPos, nextPos, heapSize;
	Key auxData;

	if (m_heap.size() > 0) {
		//The first element of the vector is replaced with the last element
		m_heap[0] = m_heap[m_heap.size() - 1];
		//Resize the vector so that now has size-1 elements (the vector removes the ones beyond and destroys them)
		m_heap.resize(m_heap.size() - 1);
	}

	heapSize = m_heap.size();

	if (m_heap.size() > 1) {
		//Until it finds the right position, check which of the children of the last element is the greater.
	//Swap the greater children with the parent
	//Repeat until you find the right position

		currentPos = 0;
		auxData = m_heap[currentPos];
		leftChild = 2 * currentPos + 1;
		rightChild = 2 * currentPos + 2;
		while (leftChild < heapSize && rightChild < heapSize) {
			//Check which of the children is the greatest. The current node will be compared to that one
			if (m_heap[leftChild] > m_heap[rightChild])
				nextPos = leftChild;
			else
				nextPos = rightChild;

			//Compare the current node to the greatest child
			if (auxData > m_heap[nextPos]) {
				break;
			}
			else {
				m_heap[currentPos] = m_heap[nextPos];		//The element must be positioned as a parent
				currentPos = nextPos;						//Now the current position will be the one of the greatest child
			}
			leftChild = 2 * currentPos + 1;					//Obtain the children indexes of the current node
			rightChild = 2 * currentPos + 2;
		}
		if (leftChild < heapSize) {
			if (auxData < m_heap[leftChild]) {
				m_heap[currentPos] = m_heap[leftChild];
				currentPos = leftChild;
			}
		}
		if (rightChild < heapSize) {
			if (auxData < m_heap[rightChild]) {
				m_heap[currentPos] = m_heap[rightChild];
				currentPos = rightChild;
			}
		}
		//Store the auxiliary value in the correct position
		m_heap[currentPos] = auxData;
	}

}
*/
