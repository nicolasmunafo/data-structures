#pragma once

#include <iostream>

template<class T> class BinarySearchTree;

template<class T>
class BinaryTree
{
	friend class BinarySearchTree<T>;
public:
	BinaryTree(T newData) : m_left(nullptr), m_right(nullptr), m_parent(nullptr), m_data(newData) {}

	~BinaryTree() {
		Destroy();
	}

	//Destroy the node and its children
	void Destroy() {
		if (m_left != nullptr) {
			delete m_left;				//It is unnecessary to call m_left->Destroy() because the deletion will destroy it recursively
			m_left = nullptr;
		}
		if (m_right != nullptr) {
			delete m_right;
			m_right = nullptr;
		}
	}

	//Recursively count the number of nodes the tree has
	int Count() {
		int nodesNum = 1;
		if (m_left != nullptr) {
			nodesNum += m_left->Count();
		}
		if (m_right != nullptr) {
			nodesNum += m_right->Count();
		}
		return nodesNum;
		
	}

	void SetLeftChild(BinaryTree<T>* newChild) {
		if (m_left == nullptr) {
			m_left = newChild;
		}
	}

	void SetRightChild(BinaryTree<T>* newChild) {
		if (m_right == nullptr) {
			m_right = newChild;
		}
	}

	T& GetData() {
		return m_data;
	}

	BinaryTree<T>* GetLeftChild() {
		if (m_left != nullptr) {
			return m_left;
		}
	}

	BinaryTree<T>* GetRightChild() {
		if (m_right != nullptr) {
			return m_right;
		}
	}

	void PreOrder() {
		std::cout << " " << m_data << " ";
		if (m_left !=nullptr) m_left->PreOrder();
		if (m_right != nullptr) m_right->PreOrder();
	}

	void InOrder() {
		if (m_left != nullptr) m_left->InOrder();
		std::cout << " " << m_data << " ";
		if (m_right != nullptr) m_right->InOrder();
	}

	void PostOrder() {
		if (m_left != nullptr) m_left->PostOrder();
		if (m_right != nullptr) m_right->PostOrder();
		std::cout << " " << m_data << " ";
	}

private:
	BinaryTree<T>* m_left;
	BinaryTree<T>* m_right;
	BinaryTree<T>* m_parent;
	T m_data;
};

