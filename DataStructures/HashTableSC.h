#pragma once
#include "HashItem.h"
#include <list>
#include <iterator>

using namespace std;		//To use the standard lists

//Hash tables implemented with Separate Chaining
//objects will never collide because they are placed in a link list associated to the hash value
template<class T>
class HashTableSC
{
public:
	HashTableSC<T>(int size) : m_size(0) {
		if (size > 0) {
			m_size = GetNextPrimeNum(size);
			m_hashTable = new list< HashItem<T> >[m_size];	//It creates an array of Link Lists with hash items
		}
	}

	~HashTableSC() {
		if (m_hashTable != nullptr) {
			delete[] m_hashTable;
			m_hashTable = nullptr;
		}
	}

	//Calculates the hash value of the key in this way: KEY mod SIZE. This ensures no collisions when SIZE is prime
	int HashFunction(int key) {
		return (key % m_size);
	}

	//Insert item in the hash table. The items are added to the list associated to the index obtained with the hash.
	//Unlike the insert in linear probing, this does not return a value because it will always be able to insert
	void Insert(int key, T& obj) {
		int hashValue = HashFunction(key);
		HashItem<T> newHashItem;
		newHashItem.SetData(obj);
		newHashItem.SetKey(key);

		//Push the item at the back of the list in the index position
		m_hashTable[hashValue].push_back(newHashItem);
	}

	//Delete an item from the table. First it must calculate the hash key and then sequentially
	//look for the item in the link list. If found it must remove the node from the list
	void Delete(int key) {
		int hashValue = HashFunction(key);
		list<HashItem<T>>* listPtr = &m_hashTable[hashValue];		//Store a reference to the list not to repeat m_hashTable[HashValue]
		
		//iterator is a struct of the STL. This iterator will start at the beginning of the list associated to the index
		typename list<HashItem<T> >::iterator hashListIt;
		hashListIt = listPtr->begin();

		//While the iterator is different to the end of the list
		//The end method returns an iterator referring to the past-the-end element in the list container
		while (hashListIt != listPtr->end()) {
			//If the item is found erase the value
			if (hashListIt->GetKey() == key) {

				//list comes with the erase method allowing you to delete an element pointed by an iterator
				listPtr->erase(hashListIt);
				printf("%s", "Element deleted");
				return;
			}
			hashListIt++;
		}
	}

	//Look for an item in the table. First it must calculate the hash key and then sequentially
	//look for the item in the link list using an iterator
	bool Find(int key, T* obj) {
		int hashValue = HashFunction(key);
		list<HashItem<T>>* listPtr = &m_hashTable[hashValue];		//Store a reference to the list not to repeat m_hashTable[HashValue]

		//iterator is a struct of the STL. This iterator will start at the beginning of the list associated to the index
		typename list<HashItem<T> >::iterator hashListIt;
		hashListIt = listPtr->begin();

		//While the iterator is different to the end of the list
		//The end method returns an iterator referring to the past-the-end element in the list container
		while (hashListIt != listPtr->end()) {
			//If the item is found return the value in the obj variable
			if (hashListIt->GetKey() == key) {
				if (obj != nullptr) {
					*obj = hashListIt->GetData();
				}
				return true;
			}
			hashListIt++;
		}

		return false;		//The item was not found
	}
	

	

private:
	
	int m_size;
	list< HashItem<T> >* m_hashTable;		//The hash table will point to a link list which will have hash items of type T

	//This function sequentially looks for a number "i" that may divide the number "num" until i^2 >num
	bool IsNumPrime(int num) {
		for (int i = 2; (i * i) <= num; i++) {
			if (num % i == 0) {
				return false;
			}
		}
		return true;
	}

	//Returns a prime number starting from the next number
	int GetNextPrimeNum(int num) {
		int i;
		for (i = num + 1; ; i++) {
			if (IsNumPrime(i)) {
				break;
			}
		}
		return i;
	}
};

