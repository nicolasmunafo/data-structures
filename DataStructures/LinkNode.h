#pragma once

//#include "LinkList.h"
//#include "LinkIterator.h"
//libraries do not work while including template classes

template<typename T> class LinkIterator;
template<typename T> class LinkList;

template<class T>
class LinkNode
{
	friend class LinkList<T>;
	friend class LinkIterator<T>;

private:
	LinkNode() :ptrPrev(nullptr), ptrNext(nullptr) {

	}
	
	LinkNode* ptrNext;

	//To give support to double linked lists
	LinkNode* ptrPrev;

	T nodeData;
};

