#pragma once
#include <assert.h>


template<class T>
class UnorderedArray
{


public:

	//Constructor
	UnorderedArray(int size, int growBy = 1);

	//Destructor
	~UnorderedArray();

	//Insert an item at the end of the array
	virtual void push(T item);

	//Remove the last item
	virtual void pop();

	//Remove an item at a certain position
	virtual void remove(int index);

	//Clear the whole array
	virtual void clear();

	//It will be a linear search. Returns -1 if it didn't find the item
	int searchItem(T item);

	//Should we use delegates to check how it will be ordered???
	/*bool orderArray();*/

	int GetSize() {
		return numElements;
	}

	
	int numElements;	//Current number of elements

	int maxSize;	//Maximum size allowed by the array

	virtual T& operator[](int index);


private:

	//pointer to a T* array
	T* uArray;
	
	int growSize;		//Growth of the array if expanded


	//Used to expand the array in case the space is not enough
	bool expand();


protected:

};


/*: m_array(nullptr), maxSize(0),growSize(0), numElements(0)*/


/*Template functions, including member functions, must be written entirely in the header files. 
This means that if you have a template class, its implementation must be entirely in a header file. This is because the compiler needs to have access to the entire 
template definition (not just the signature) in order to generate code for each instantiation of the template.*/

template <class T> 
UnorderedArray<T>::UnorderedArray(int size, int growBy):
	uArray(nullptr), maxSize(0), growSize(0), numElements(0)
{

	if (size > 0) {
		maxSize = size;
		growSize = (growBy > 0 ? growBy : 0);
		uArray = new T[maxSize];
		numElements = 0;
	}
}

template <class T> 
UnorderedArray<T>::~UnorderedArray() {
	if (uArray != nullptr) {
		delete uArray;
		uArray = nullptr;
	}
}

template <class T> 
void UnorderedArray<T>::push(T item) {
	if (uArray != nullptr) {
		if (numElements >= maxSize) {
			expand();
		}
		uArray[numElements] = item;
		numElements++;

	}
}

template<class T>
inline void UnorderedArray<T>::pop()
{
	if (numElements > 0) {
		numElements--;
	}
}


/*Removes an index, regardless of it having or not a value*/
template<class T>
inline void UnorderedArray<T>::remove(int index) {

	//Only execute it if the element exists, that is, if the index is less than numElements (I can't get to work that (uArray + index)!=nullptr
	if (index >= 0 && index <= maxSize) {

		//This loop will overwrite each element with the following one starting from the index.
		//Up to the number of elements minus 1 because the last element will not be overwritten
		//If the element is the last in the list, that is index+1 == numElements => the loop will not execute
		for (int i = index; i < (maxSize - 1); i++) {
			uArray[i] = uArray[i + 1];
		}

		maxSize--;		//It has to reduce the number of elements because it removed an element, so the array will not get to that element

		if (numElements > maxSize) {
			numElements = maxSize;
		}
	}
}

template<class T>
inline void UnorderedArray<T>::clear() {
	numElements = 0;
}

template<class T>
bool UnorderedArray<T>::expand() {
	//Will only grow if the growth size is bigger than 0
	if (growSize > 0) {
		T* newArray = new T[maxSize + growSize];	//creates a new array with the current maxsize + the growth size


		memcpy(newArray, uArray, sizeof(T) * maxSize);
		/*for (int i = 0; i <= numElements; i++) {
			newArray[i] = uArray[i];
		}*/

		delete[] uArray;		//deletes the previously allocated memory. 
		//delete uArray also works BUT it's incorrect, because it doesn't deallocate every element of the array, only the first element

		maxSize += growSize;	//now the max size is the maxsize+growth size

		uArray = newArray;		//the field pointer will now have the new array

		return true;
	}
	else
		return false;
}

template<class T>
T& UnorderedArray<T>::operator[](int index)
{
	assert(uArray != nullptr && index <= numElements);
	//if (uArray != nullptr && index <= numElements) {
		return uArray[index];
	//}
}

template<class T>
int UnorderedArray<T>::searchItem(T item) {
	assert(uArray != nullptr);
	int i = 0;
	while (i < numElements && uArray[i] != item) {
		i++;
	}
	return (i < numElements ? i : -1);


	/*Another way
	if (uArray != nullptr) {
		for (int i=0; i<numElements; i++){
			if (uArray[i] == item){
				return i;
			}
		}
		return -1; 
	}
	
		
	*/

}








/*This function removes items only considering items with a current value

template<class T>
inline void UnorderedArray<T>::remove(int index) {

	//Only execute it if the element exists, that is, if the index is less than numElements (I can't get to work that (uArray + index)!=nullptr
	if (index>=0 && index<=numElements) {

		//This loop will overwrite each element with the following one starting from the index.
		//Up to the number of elements minus 1 because the last element will not be overwritten
		//If the element is the last in the list, that is index+1 == numElements => the loop will not execute
		for (int i = index; i < (numElements - 1); i++) {
			uArray[i] = uArray[i + 1];
		}

		numElements--;		//It has to reduce the number of elements because it removed an element, so the array will not get to that element
	}

}*/