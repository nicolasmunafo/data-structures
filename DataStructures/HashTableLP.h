#pragma once
#include "HashItem.h"
#include <iostream>

using namespace std;

//Hash table with linear probing
//If the hash function found a collision, it will search sequentially from that position until it finds an empty index

template<class T>
class HashTableLP
{
public:

	HashTableLP(int size) : m_size(0), m_totalItems(0) {
		if (size > 0) {
			m_size = GetNextPrimeNum(size);				//SIZE must be prime so as to avoid collisions when calculating the has value (because hash values are calculated with modulo)
			m_hashTable = new HashItem<T>[m_size];		//The table will point to the first position of an array of Hash Items
		}
	}

	~HashTableLP() {
		if (m_hashTable != nullptr) {
			delete[] m_hashTable;
			m_hashTable = nullptr;
		}
	}

	//Calculates the hash value of the key in this way: KEY mod SIZE. This ensures no collisions when SIZE is prime
	int HashFunction(int key) {
		return (key % m_size);
	}

	//Overloading of HashFunction, calculates the hash value of a string key
	int HashFunction(string key) {
		int hashVal = 0;

		for (int i = 0; i < (int)key.size();i++) {
			int charVal = (int)key[i];
			hashVal = HashFunction(hashVal * 256 + charVal);
		}
		return hashVal;
	}

	//Insert an item in the hash table using open addressing - Linear probing to solve collisions
	//Returns false if it wasn't able to insert the item, or true otherwise
	bool Insert(int key, T &obj) {
		//Check first if there is space. If not, return false
		if (m_totalItems == m_size) {
			return false;
		}

		//Calculate the hash value associated to the key
		int hashValue = HashFunction(key);

		//While the key of the current position is not empty
			//increment the hashvalue and calculate its modulo m_size.
			//This is because positions always have to be between 0 and the size of the array.
			//In this way, if the array is of size 8, when it gets to position 7, 7+1= 8 and 8 mod 8 = 0 and it starts again
			//This loop is NOT infinite because the insert first checks if there is space
		//If the key is empty it will exit the loop
		while (m_hashTable[hashValue].GetKey() != -1) {
			hashValue++;
			hashValue = HashFunction(hashValue);		//The text says hash%= m_size, but this is just the hash function. And what if it changes?
		}

		//Insert the value in the position
		m_hashTable[hashValue].SetKey(key);
		m_hashTable[hashValue].SetData(obj);

		//Increment the number of items
		m_totalItems++;
		
		//Return true because it was able to insert to the item
		return true;
	}

	void Delete(int key) {
		//Calculate the hash value of the key
		int hashValue = HashFunction(key);
		int initialHash = hashValue;

		//Search the item until it gets to an invalid position
		do
		{
			if (m_hashTable[hashValue].GetKey() == key) {	//Check if the key of the position is the required one
				m_hashTable[hashValue].SetKey(-1);			//Invalidates the key if the element is the one
				m_totalItems--;								//Decrements the amount of items because it deleted it
				return;										//Finishes the function. The element has been deleted
			}

			hashValue++;									//Goes to the next hash value
			hashValue = HashFunction(hashValue);						//Calculates the hash function of the value
		} while (hashValue != initialHash);					//If the hash value is the first initial hash, it must finish because it has cycled throughout the whole table

	}

	//Searches the object and if it is found it will return the data in a pointer of type T
	bool Find(int key, T *obj) {
		//Calculate the hash value of the key
		int hashValue = HashFunction(key);
		int initialHash = hashValue;				//Store the initial value to check later if it hasn't returned to the start of the search

		//Search the item until it gets to an invalid position
		do
		{
			if (m_hashTable[hashValue].GetKey() == key) {	//Check if the key of the position is the required one
				*obj = m_hashTable[hashValue].GetData();	//Stores the content of the data in a pointer of type T
				return true;										//Finishes the function and returns true because it found the value
			}

			hashValue++;									//Goes to the next hash value
			hashValue = HashFunction(hashValue);			//Calculates the hash function of the value
		} while (hashValue != initialHash);					//If the hash value is the first initial hash, it must finish because it has cycled throughout the whole table
		
		return false;										//If it exits the loop, it wasn't able to find the value
	}

	int GetTotalItems() {
		return m_totalItems;
	}

	int GetSize() {
		return m_size;
	}

	HashItem<T>* m_hashTable;		//DELETE

private:

	//HashItem<T>* m_hashTable;		//The table will point to a hash item
	int m_size, m_totalItems;

	//If there is an integer number m such that 2<= m <= sqrt(NUM) (or 4<= m * m <= NUM) 
	//AND NUM mod m = 0 => NUM is not prime
	//This function sequentially looks for a number "i" that may divide the number "num" until i^2 >num
	bool IsNumPrime(int num) {
		for (int i=2; (i * i) <= num; i++) {
			if (num % i == 0) {
				return false;
			}
		}
		return true;
	}

	//Returns a prime number starting from the next number
	int GetNextPrimeNum(int num) {	
		int i;
		for (i = num + 1; ; i++) {
			if (IsNumPrime(i)) {
				break;
			}
		}
		return i;
	}



};



/*
void Delete(int key) {
		//Calculate the hash value of the key
		int hashValue = HashFunction(key);
		int initialHash = hashValue;

		//Search the item until it gets to an invalid position
		while (m_hashTable[hashValue].GetKey() != -1) {
			if (m_hashTable[hashValue].GetKey() == key) {	//Check if the key of the position is the required one
				m_hashTable[hashValue].SetKey(-1);			//Invalidates the key if the element is the one
				m_totalItems--;								//Decrements the amount of items because it deleted it
				return;										//Finishes the function. The element has been deleted
			}

			hashValue++;									//Goes to the next hash value
			hashValue = HashFunction(hashValue);						//Calculates the hash function of the value
			if (hashValue == initialHash) {					//If the hash value is the first initial hash, it must finish because it has cycled throughout the whole table
				return;
			}

		}
	}

The problem with this definition is the following. Suppose you create the table
51
6
11
21
36

And delete 11 then

51
6
-1
21
36

Ifyou want to delete 51, first checks 6. as 6!=51, then goes to -1 so it exits the loop, but the value was there!
To me the condition of the while has to be different, until it has reached the beginning


*/